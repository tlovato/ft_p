/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   download_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 11:18:58 by tlovato           #+#    #+#             */
/*   Updated: 2019/04/12 11:19:00 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_p.h"

static int			error(char *message)
{
	printf("ERROR : %s\n", message);
	return (-1);
}

static char			*get_content(int fd)
{
	char			buf;
	char			*cat;
	char			*newstock;

	newstock = ft_strnew(1);
	ft_bzero(newstock, 1);
	while (read(fd, &buf, 1))
	{
		cat = ft_strnew(1);
		cat[0] = buf;
		newstock = ft_strjoinfree(newstock, cat, 3);
	}
	return (newstock);
}

static int			check_fd(char *name, int clientfd)
{
	int				fd;

	if ((fd = open(name, O_RDWR)) == -1)
	{
		write(clientfd, "FAIL\0", 5);
		return (0);
	}
	else
		write(clientfd, "SUCC\0", 5);
	return (fd);
}

int					download_file(int clientfd, int sockfd)
{
	size_t			len;
	char			*name;
	char			*content;
	int				fd;
	struct stat		fs;

	read(clientfd, &len, sizeof(len));
	name = (char *)malloc(sizeof(char) * len + 1);
	ft_bzero(name, len + 1);
	read(clientfd, name, len);
	if (!(fd = check_fd(name, clientfd)))
		return (error("file not found."));
	fstat(fd, &fs);
	len = fs.st_size;
	write(clientfd, &len, sizeof(len));
	content = get_content(fd);
	write(clientfd, content, ft_strlen(content));
	printf("File [%s] downloaded\n", name);
	free(name);
	free(content);
	return (0);
}
