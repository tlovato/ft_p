/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   upload_file.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 11:19:37 by tlovato           #+#    #+#             */
/*   Updated: 2019/04/12 11:19:41 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_p.h"

static void				create_dir(void)
{
	DIR					*dir;

	if (!(dir = opendir("upload")))
		mkdir("upload", 0777);
	else
		closedir(dir);
}

static void				add_file(char *name, char *content, size_t len)
{
	int					fd;
	char				*path;

	path = ft_strjoin("upload/", name);
	fd = open(path, O_CREAT | O_RDWR);
	write(fd, content, len);
	free(path);
	close(fd);
}

int						upload_file(int clientfd, int sockfd)
{
	char				*name;
	size_t				name_len;
	char				*content;
	size_t				content_len;
	char				buf[5];

	ft_bzero(buf, 5);
	read(clientfd, buf, 5);
	if (!ft_strcmp(buf, "FAIL"))
		return (-1);
	create_dir();
	read(clientfd, &name_len, sizeof(name_len));
	name = (char *)malloc(sizeof(char) * name_len + 1);
	ft_bzero(name, name_len + 1);
	read(clientfd, name, name_len);
	read(clientfd, &content_len, sizeof(content_len));
	content = (char *)malloc(sizeof(char) * content_len + 1);
	ft_bzero(content, content_len + 1);
	read(clientfd, content, content_len);
	add_file(name, content, content_len);
	printf("File [%s] uploaded\n", name);
	free(name);
	free(content);
	return (0);
}
