/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serveur.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 11:19:31 by tlovato           #+#    #+#             */
/*   Updated: 2019/04/12 11:19:32 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_p.h"

static int				usage(char *cmd)
{
	printf("usage : %s <port>\n", cmd);
	return (-1);
}

static int				create_server(int port)
{
	int					sockfd;
	struct protoent		*proto;
	struct sockaddr_in	sin;

	if (!(proto = getprotobyname("tcp")))
		return (-1);
	sockfd = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	bind(sockfd, (const struct sockaddr *)&sin, sizeof(sin));
	listen(sockfd, 42);
	return (sockfd);
}

static void				receive_action(int clientfd, int sockfd)
{
	char				buf[4];

	while (1)
	{
		ft_bzero(buf, 4);
		read(clientfd, &buf, 3);
		if (!ft_strcmp(buf, "PUT"))
			upload_file(clientfd, sockfd);
		else if (!ft_strcmp(buf, "GET"))
			download_file(clientfd, sockfd);
		else if (ft_strcmp(buf, "EXI"))
		{
			printf("Client Exited\n");
			break ;
		}
	}
}

int						main(int ac, char **av)
{
	int					fd[2];
	struct sockaddr_in	csin;
	unsigned int		cslen;
	char				buf[20];
	pid_t				pid;

	if (ac != 2)
		return (usage(av[0]));
	if ((fd[0] = create_server(ft_atoi(av[1]))) == -1)
		return (-1);
	printf("Listenting on port [%d]\n", ft_atoi(av[1]));
	while (1)
	{
		fd[1] = accept(fd[0], (struct sockaddr *)&csin, &cslen);
		pid = fork();
		if (!pid)
			break ;
	}
	if (fd[1] != -1)
		printf("Connected to client\n");
	receive_action(fd[1], fd[0]);
	close(fd[0]);
	close(fd[1]);
	return (0);
}
