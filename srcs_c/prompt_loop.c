/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prompt_loop.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 11:18:38 by tlovato           #+#    #+#             */
/*   Updated: 2019/04/12 11:18:39 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_p.h"

static char			*get_line(void)
{
	char			buf;
	char			*newstock;
	char			*cat;

	newstock = ft_strnew(1);
	ft_bzero(newstock, 1);
	while (read(0, &buf, 1) && buf != '\n')
	{
		cat = ft_strnew(1);
		cat[0] = buf;
		newstock = ft_strjoinfree(newstock, cat, 3);
	}
	return (newstock);
}

static void			execution(char **cmd)
{
	pid_t			pid;

	pid = fork();
	if (!ft_strcmp(cmd[0], "ls"))
		cmd[0] = ft_strjoinfree("/bin/", cmd[0], 2);
	else if (!ft_strcmp(cmd[0], "pwd"))
		cmd[0] = ft_strjoinfree("/bin/", cmd[0], 2);
	if (!pid)
	{
		if ((execv(cmd[0], cmd)) == -1)
		{
			printf("ERROR : command not found.\n");
			exit(-1);
		}
	}
	else
		wait4(pid, 0, 0, NULL);
}

static void			cd(char **cmd)
{
	if (cmd[1])
		if ((chdir(cmd[1])) == -1)
			printf("ERROR : not a valid directory.\n");
}

void				prompt_loop(int sockfd)
{
	char			*cmd;
	char			**cmd_tab;

	while (1)
	{
		ft_putstr("$> ");
		cmd = get_line();
		cmd_tab = ft_strsplit(cmd, ' ');
		if (!ft_strcmp(cmd_tab[0], "quit"))
		{
			write(sockfd, "EXI", 3);
			exit(0);
		}
		if (!ft_strcmp(cmd_tab[0], "cd"))
			cd(cmd_tab);
		else if (!ft_strcmp(cmd_tab[0], "put"))
			put_file(cmd_tab, sockfd);
		else if (!ft_strcmp(cmd_tab[0], "get"))
			get_file(cmd_tab, sockfd);
		else
			execution(cmd_tab);
		free(cmd);
	}
}
