/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_file.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 11:18:32 by tlovato           #+#    #+#             */
/*   Updated: 2019/04/12 11:18:33 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_p.h"

static int		error(char *message)
{
	printf("ERROR : %s\n", message);
	return (-1);
}

static void		create_dir(void)
{
	DIR			*dir;

	if (!(dir = opendir("download")))
		mkdir("download", 0777);
	else
		closedir(dir);
}

static char		*strip_name(char *name)
{
	int				i;

	i = ft_strlen(name);
	while (i > 0 && name[i] != '/')
		i--;
	return (&name[i]);
}

static void		add_file(char *name, char *content, size_t len)
{
	int			fd;
	char		*path;
	char		*name_stripped;

	name_stripped = strip_name(name);
	path = ft_strjoin("download/", name_stripped);
	fd = open(path, O_CREAT | O_RDWR);
	write(fd, content, len);
	free(path);
	close(fd);
}

int				get_file(char **cmd, int sockfd)
{
	size_t		len;
	char		*content;
	char		buf[5];

	if (!cmd[1] || cmd[2])
		return (error("usage : put <file>."));
	write(sockfd, "GET", 3);
	create_dir();
	len = ft_strlen(cmd[1]);
	write(sockfd, &len, sizeof(len));
	write(sockfd, cmd[1], len);
	read(sockfd, buf, 5);
	if (!ft_strcmp(buf, "FAIL"))
		return (-1);
	read(sockfd, &len, sizeof(len));
	content = (char *)malloc(sizeof(char) * len + 1);
	ft_bzero(content, len + 1);
	read(sockfd, content, len);
	add_file(cmd[1], content, len);
	return (0);
}
