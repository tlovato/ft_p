/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 11:18:26 by tlovato           #+#    #+#             */
/*   Updated: 2019/04/12 11:18:27 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_p.h"

static int				usage(char *cmd)
{
	printf("usage : %s <addr> <port>\n", cmd);
	return (-1);
}

static int				create_client(char *addr, int port)
{
	int					sockfd;
	struct protoent		*proto;
	struct sockaddr_in	sin;

	if (!(proto = getprotobyname("tcp")))
		return (-1);
	sockfd = socket(PF_INET, SOCK_STREAM, proto->p_proto);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	sin.sin_addr.s_addr = inet_addr(addr);
	connect(sockfd, (const struct sockaddr *)&sin, sizeof(sin));
	return (sockfd);
}

int						main(int ac, char **av)
{
	int					sockfd;

	if (ac != 3)
		return (usage(av[0]));
	if ((sockfd = create_client(av[1], ft_atoi(av[2]))) == -1)
		return (-1);
	prompt_loop(sockfd);
	close(sockfd);
	return (0);
}
