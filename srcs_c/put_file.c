/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_file.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/12 11:18:44 by tlovato           #+#    #+#             */
/*   Updated: 2019/04/12 11:18:45 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_p.h"

static int			error(char *message)
{
	printf("ERROR : %s\n", message);
	return (-1);
}

static char			*get_content(int fd)
{
	char			buf;
	char			*cat;
	char			*newstock;

	newstock = ft_strnew(1);
	ft_bzero(newstock, 1);
	while (read(fd, &buf, 1))
	{
		cat = ft_strnew(1);
		cat[0] = buf;
		newstock = ft_strjoinfree(newstock, cat, 3);
	}
	return (newstock);
}

static char			*strip_name(char *name)
{
	int				i;

	i = ft_strlen(name);
	while (i > 0 && name[i] != '/')
		i--;
	return (&name[i]);
}

static int			check_fd(char *name, int sockfd)
{
	int				fd;

	if ((fd = open(name, O_RDWR)) == -1)
	{
		write(sockfd, "FAIL\0", 5);
		return (0);
	}
	else
		write(sockfd, "SUCC\0", 5);
	return (fd);
}

int					put_file(char **cmd, int sockfd)
{
	int				fd;
	char			*content;
	struct stat		fs;
	size_t			len;
	char			*name;

	if (!cmd[1] || cmd[2])
		return (error("usage : put <file>."));
	write(sockfd, "PUT", 3);
	if (!(fd = check_fd(cmd[1], sockfd)))
		return (error("file not found."));
	fstat(fd, &fs);
	name = strip_name(cmd[1]);
	len = ft_strlen(name);
	write(sockfd, &len, sizeof(len));
	write(sockfd, name, ft_strlen(name));
	len = fs.st_size;
	write(sockfd, &len, sizeof(len));
	content = get_content(fd);
	write(sockfd, content, ft_strlen(content));
	return (0);
}
