SERVEUR_NAME = serveur
CLIENT_NAME = client

S_SRCS_DIR = srcs_s/
C_SRCS_DIR = srcs_c/

S_SRCS_FILES = serveur.c upload_file.c download_file.c
C_SRCS_FILES = client.c prompt_loop.c put_file.c get_file.c

S_SRCS = $(addprefix $(S_SRCS_DIR), $(S_SRCS_FILES))
C_SRCS = $(addprefix $(C_SRCS_DIR), $(C_SRCS_FILES))

S_OBJS = $(S_SRCS:.c=.o)
C_OBJS = $(C_SRCS:.c=.o)

all: $(SERVEUR_NAME) $(CLIENT_NAME)

%.o: %.c
		gcc -g3 -c $< -o $@

$(SERVEUR_NAME): $(S_OBJS)
		make -C libft/ 
		gcc -g3 -Wall -Wextra -Werror -o $@ $^ -L libft/ -lft -fsanitize=address

$(CLIENT_NAME): $(C_OBJS)
		make -C libft/ 
		gcc -g3 -Wall -Wextra -Werror -o $@ $^ -L libft/ -lft -fsanitize=address

clean:
		rm -f $(S_OBJS) $(C_OBJS)
		cd libft/ && make clean

fclean: clean
		rm -rf $(SERVEUR_NAME) $(CLIENT_NAME)
		cd libft/ && make fclean

re: fclean all

.PHONY: all clean fclean re build

